import React, { createContext, Component } from 'react'
import PropTypes from 'prop-types'
import filetype from 'file-type'

import { withPath } from '@ipui/path'
import { withIpfs } from '@ipui/ipfs'

const SourceContext = createContext()

class Source extends Component {

  state = {
    source: null,
    stats: null,
    ildeTime: 5,
    tickInterval: null,
    isNew: false
  }

  static propTypes = {
    children: PropTypes.node.isRequired,
    newFile: PropTypes.elementType
  }

  constructor( props ) {
    super( props )

    this.load = this.load.bind( this )
    this.loadFile = this.loadFile.bind( this )
    this.update = this.update.bind( this )
    this.tick = this.tick.bind( this )

    this.rename = this.rename.bind( this )
    this.save = this.save.bind( this )
    this.forceSave = this.forceSave.bind( this )
  }

  componentDidMount() {
    const { current } = this.props.withPath
    this.load( current )
  }

  load( path ) {
    const { stat } = this.props.withIpfs.node.files
    const { filename, dirname } = this.props.withPath
    const { loadFile } = this

    stat( path )

      .then( stats => {

        const { type } = stats
        switch( type ) {

          case 'directory':
            this.setState( {
              ...this.state,
              stats: {
                ...stats,
                fullpath: path,
                dirname: path
              }
            } )
            break

          case 'file':
            this.setState( {
              ...this.state,
              stats: {
                ...stats,
                fullpath: path,
                dirname: dirname( path ),
                filename: filename( path )
              }
            } )
            loadFile( path )
            break

          default:
            console.error( 'unhandled file type', path )

        }


      } )

      .catch( err => {
        const errorMessage = err.toString()
        if( errorMessage === 'Error: file does not exist' ) {
          this.setState( {
            ...this.state,
            isNew: true
          } )
        }
        console.error( 'load loremso', err )
      } )
  }

  loadFile( path ) {
    const { read } = this.props.withIpfs.node.files

    read( path, { offset: 0, length: 32 } )

      .then( header => {
        const type = filetype( header )
        if ( type )
          return new Error( `unhandled filetype: ${ type.mime }` )

        return read( path )
      } )

      .then( fileBuf => {
        this.setState( {
          ...this.state,
          source: fileBuf.toString( 'utf8' )
        } )
      } )

      .catch( error => {
        console.error( 'header', error )
      } )

  }

  save() {
    const { fullpath } = this.state.stats
    const { source, tickInterval } = this.state
    const { write } = this.props.withIpfs.node.files

    clearInterval( tickInterval )

    write( fullpath, Buffer.from( source ), {
      create: true,
      truncate: true
    } )

      .then( () => {
        console.log( 'save and reset' )
        this.setState( {
          ...this.state,
          ildeTime: 5,
          tickInterval: null
        } )
      } )

      .catch( error => {
        console.error( 'write', error )
      } )
  }

  tick() {
    const { ildeTime } = this.state
    const { save } = this

    if ( ildeTime === 0 ) {
      save()
    } else {

      this.setState( {
        ...this.state,
        ildeTime: ildeTime - 1
      } )

    }
  }

  update( source ) {
    const { tickInterval } = this.state
    const { type } = this.state.stats

    this.setState( {
      ...this.setState,
      source: source,

      ildeTime: 5,
      tickInterval: type === 'file' ? tickInterval || setInterval( this.tick, 1000 ) : null
    } )
  }

  rename( newName ) {
    const { mv, write } = this.props.withIpfs.node.files
    const { fullpath, dirname, type } = this.state.stats
    const { source } = this.state
    const { join, go } = this.props.withPath
    const { load } = this
    let newPath

    if ( type === 'directory' ) {
      newPath = join( dirname, newName )
      return new Promise( ( resolve, reject ) => {
        write( newPath, Buffer.from( source ), {
          create: true,
          parents: true
        }, err => {
          if ( err )
            return reject( err )

          load( newPath )
          go( newPath )
          resolve( newPath )
        } )
      } )
    } else {

      newPath = join( dirname, newName )
      return new Promise( ( resolve, reject ) => {
        mv( fullpath, newPath, err => {
          if ( err )
            return reject( err )

          load( newPath )
          go( newPath )
          resolve( newPath )
        } )
      } )

    }
  }

  forceSave() {
    this.save()
  }

  render() {
    const { source, stats, isNew, ildeTime } = this.state
    const { update, rename, forceSave } = this
    const { children, newFile } = this.props

    if ( isNew ) {
      const element = newFile ? React.createElement( newFile ) : ( <>new file</> )
      return ( <>{ element }</> )
    }

    if ( !stats )
      return ( <>loading file</> )

    return (
      <SourceContext.Provider value={ {
        ildeTime,
        stats,
        source,
        update,

        rename,
        forceSave
      } } >
        { children }
      </SourceContext.Provider>
    )
  }
}

const withSource = ( ComponentAlias ) => {

  return props => (
    <SourceContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withSource={ context } />
      } }
    </SourceContext.Consumer>
  )

}

export default withPath( withIpfs( Source ) )

export {
  SourceContext,
  withSource
}
