import React from 'react'

import { withSource } from '../../context/source/Source'
import { FiSave, FiCheck } from 'react-icons/fi'

import './Save.sass'

const FileSave = props => {
  const { ildeTime, forceSave } = props.withSource

  return (
    <button className="save" onClick={ forceSave }>
      <FiSave />

      <sup>
        { ildeTime < 5 && ildeTime > 0 ? (
          <>
            { ildeTime }
          </>
        ) : (
          <FiCheck />
        ) }
      </sup>
    </button>
  )
}

export default withSource( FileSave )
