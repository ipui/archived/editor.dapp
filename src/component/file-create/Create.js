import React from 'react'

import { withPath } from '@ipui/path'
import { withIpfs } from '@ipui/ipfs'

import './Create.sass'

const FileCreate = props => {

  function handleCreate() {
    const { current, redirect } = props.withPath
    touch( current )
      .then( () => {
        redirect( current )
      } )
      .catch( err => {
        console.error( 'touch', err )
      } )
  }

  function touch( path ) {
    const { write } = props.withIpfs.node.files
    return write( path, Buffer.from( '' ), {
      create: true,
      parents: true
    } )
  }

  function handleGoFilesDapp() {
    const { api, current, dirname } = props.withPath
    const { stat } = props.withIpfs.node.files

    let dir = dirname( current )

    stat( dir, ( err, stats ) => {
      if ( err )
        dir = '/'

      const redirectPath = 'http://127.0.0.1:8080/ipns/12D3KooWCcbggDkc8B25u51XktYSBv1b8g9W31Zii5ovyJPHJ86X/#' + api + dir
      window.location.replace( redirectPath )
    } )
  }

  return (
    <section className="file-create">
      <p>
        file not found, what do you want to do?
      </p>
      <menu>
        <button onClick={ handleCreate }>
          create
        </button>
        <label>
          or
        </label>
        <button onClick={ handleGoFilesDapp }>
          go to directory
        </button>
      </menu>
    </section>
  )
}

export default withPath( withIpfs( FileCreate ) )
