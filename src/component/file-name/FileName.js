import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { FiCheck } from 'react-icons/fi'

import { withSource } from '../../context/source/Source'

const Name = props => {

  const [ newName, setNewName ] = useState( '' )

  const { filename } = props.withSource.stats
  const { isToggled } = props

  function handleRename() {
    const { rename } = props.withSource
    rename( newName )
      .then( () => {
        reset()
      } )
      .catch( err => {
        console.error( 'could not rename the file', err )
      } )
  }

  function reset() {
    const { toggle } = props
    setNewName( '' )
    toggle()
  }

  function updateNewName( event ) {
    setNewName( event.target.value )
  }

  return (
    <>
      { isToggled ? (
        <>
          <input
            value={ newName }
            placeholder={ filename || 'untitled' }
            onChange={ updateNewName }
            onKeyDown={ e => {
              if ( e.keyCode === 13 )
                handleRename()
            } }
            size="1"
          />
          <button onClick={ handleRename }>
            <FiCheck />
          </button>
        </>
      ) : (
        <h1>{ filename || 'untitled' }</h1>
      ) }
    </>
  )
}

Name.propTypes = {
  isToggled: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
}

export default withSource( Name )
