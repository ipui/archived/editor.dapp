import React from 'react'

import Path from '@ipui/path'
import Ipfs from '@ipui/ipfs'

import Settings from '../settings/Settings'
import Editor from '../editor/Editor'
import Toolbar from '../toolbar/Toolbar'
import FileCreate from '../file-create/Create'

import NoIpfs from '../no-ipfs/NoIpfs'

import Source from '../../context/source/Source'

const Main = props => {

  return (
    <Path>
      <Ipfs noIpfs={ NoIpfs }>
        <Source newFile={ FileCreate }>

        <Settings />

        <Editor />

        <Toolbar />

        </Source>
      </Ipfs>
    </Path>
  )
}

export default Main
