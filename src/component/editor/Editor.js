import React, { Component } from 'react'

import { Controlled as CodeMirror } from 'react-codemirror2'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/midnight.css'
import 'codemirror/mode/javascript/javascript'
import 'codemirror/mode/css/css'

import WhichLang from '../../lib/whichLang/WhichLang'
import javascript from './conf.d/javascript'
import css from './conf.d/css'

import { withSource } from '../../context/source/Source'

class Editor extends Component {

  state = {
    language: ''
  }

  constructor( props ) {
    super( props )
    this._update = this._update.bind( this )
    this.watch = this.watch.bind( this )
    this.which = new WhichLang( [
      javascript,
      css
    ] )
  }

  _update( editor, data, source ) {
    const { update } = this.props.withSource
    update( source )
  }

  watch( editor, data, source ) {
    const lang = this.which.from( source )
    this.setState( { ...this.state, language: lang.name } )
  }

  render() {
    const { _update, watch } = this
    const { language } = this.state
    const { source } = this.props.withSource

    return (
      <main>
        <CodeMirror
          value={ source }
          options={ {
            mode: language,
            theme: 'midnight',
            lineNumbers: true,
          } }
          onBeforeChange={ _update }
          onChange={ watch }
        />
      </main>
    )
  }

}

export default withSource( Editor )
