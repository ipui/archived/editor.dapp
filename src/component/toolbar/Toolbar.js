import React from 'react'

import OpenDir from '../toolbar-opendir/OpenDir'
import FileSave from '../file-save/Save'

const Toolbar = props => {

  return (
    <footer>

      <OpenDir />

      <FileSave />

      { /*
      <div className="group">
        <select size="1">
          <option>javascript</option>
          <option>css</option>
          <option>sass</option>
          <option>coffeescript</option>
        </select>
      </div>
        */ }
    </footer>
  )
}

export default Toolbar
