import React from 'react'

import { FiFolder } from 'react-icons/fi'

import { withPath } from '@ipui/path'
import { withSource } from '../../context/source/Source'

const Menu = props => {

  function openDir() {
    const { api, current, dirname } = props.withPath
    const { type } = props.withSource.stats
    let dir = dirname( current )
    if ( type === 'directory' )
      dir = current

    const redirectPath = 'http://127.0.0.1:8080/ipns/12D3KooWCcbggDkc8B25u51XktYSBv1b8g9W31Zii5ovyJPHJ86X/#' + api + dir
    window.location.replace( redirectPath )
  }

  return (
    <button onClick={ openDir }>
      <FiFolder />
    </button>
  )
}

export default withPath( withSource( Menu ) )
