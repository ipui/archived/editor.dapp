import Analyzer from './Analyzer'
import Language from './Language'

class WhichLang {

  constructor( analyzers ) {
    this.analyzers = []
    this.loadAnalyzers( analyzers )
  }

  loadAnalyzers( analyzers ) {
    analyzers.forEach( ( analyzer ) => {
      if ( this.isAnalyzer ) {
        this.analyzers = [
          ...this.analyzers,
          analyzer
        ]
      }
    } )
  }

  isAnalyzer( analyzer ) {
    return analyzer ? analyzer instanceof Analyzer : false
  }

  from( source ) {
    return this.analyzers.reduce( ( lang, analyzer ) => {
      const stats = analyzer.stats( source )
      if ( stats > lang.score )
        return { name: analyzer.name, score: stats }

      return lang
    }, { name: undefined, score: 0 } )
  }

}

export default WhichLang

export {
  Language,
  Analyzer
}

