class Analyzer {
  constructor( language ) {
    const { rules, name } = language
    if ( !rules || !name )
      throw new Error( 'malformed language' )

    this.rules = rules
    this.name = name
  }

	calculateAmount( touches ) {
    if ( touches === 0 )
      return 0

		const amount = 1 / touches
    return amount + this.calculateAmount( touches - 1 )

	}

	touch( rule, source ) {
		const matches = source.match( rule )

		if ( !matches )
			return 0

		return this.calculateAmount( matches.length )

	}

  stats( source ) {
    if ( !source )
      return
      // TODO console.error( 'source code is required' )

		return this.rules.reduce( ( score, rule ) => {
			const amount = this.touch( rule, source )
      return score + amount
		}, 0 )
  }

}

export default Analyzer

