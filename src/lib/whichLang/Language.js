class Language  {

	constructor( name, rules ) {
		this.name = name
    this.rules = []
		this.loadRules( rules || [] )
	}

  loadRules( rules ) {
    rules.forEach( ( rule ) => {
      this.register( rule )
    } )
  }

	isValidRule( rule ) {
		return rule ? rule instanceof RegExp : false
	}

	register( rule ) {
    if ( !this.isValidRule( rule ) )
      throw new Error( 'invalid rule' )

		this.rules = [
			...this.rules,
			rule
		]
	}

}

export default Language

