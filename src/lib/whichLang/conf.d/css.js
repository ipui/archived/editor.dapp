import { Language, Analyzer } from '../../../lib/whichLang/WhichLang'

const css = new Language( 'css', [
  /color/g,
  /height/g,
  /height/g,
  /\/\/\*[\s\n]/g
] )

export default new Analyzer( css )

