import { Language, Analyzer } from '../../../lib/whichLang/WhichLang'

const javascript = new Language( 'javascript', [
  /await/g,
  /console[\s\n]*[.][\s\n]*log[\s\n]*\(/g,
  /var[\s\n]+/g,
  /function[\s\n]*[\w]*[\s\n]*\(/g,
  /document[\s\n]*[.]/g,
  /\/\*/g,
  /\*\//g,
  /\/\/.*\n/g
] )

export default new Analyzer( javascript )

